import React from 'react';
import items from "../../items";

const OrderSum = ({order, remove}) => {
    const orderItems = [];
    let totalPrice = 0

    Object.keys(order).forEach(itemKey => {
        const itemSelected = order[itemKey] > 0;

        if (itemSelected) {
            const itemPrice = items[itemKey].price * order[itemKey];

            orderItems.push((
                <div key={itemKey}>
                    {items[itemKey].title} x {order[itemKey]} = {itemPrice}
                    <button className="btn-remove" onClick={() => remove(itemKey)}><strong>X</strong></button>
                </div>
            ));

            totalPrice += itemPrice;
        }
    });
    return totalPrice > 0 ? (
        <div>
            {orderItems}
            <p><strong>Total price:</strong> {totalPrice}</p>
        </div>
    ) : (
        <p>Order is not selected!<br/>Please add some items!</p>
    );
};

export default OrderSum;