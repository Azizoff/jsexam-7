import React from 'react';

const ItemOrder = ({item, add}) => {
    const {icon, title, price} = item;

    return (
        <div className="card" onClick={add}  style={{border: 'none'}}>
            <div className="row">
                <div className="col-auto">
                    <img style={{width: 60}} src={icon} alt={title}/>
                </div>
            </div>

            <div className="row">
                <div className="col">
                    <p><strong>{title}</strong></p>
                    <p>Price: {price} KGS</p>
                </div>
            </div>
        </div>
    );
};

export default ItemOrder;