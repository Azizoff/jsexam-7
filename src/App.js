import React, {useState} from "react";
import './App.css';
import items from "./items";
import ItemOrder from "./components/itemOrder/itemOrder";
import OrderSum from "./components/OrderSum/OrderSum";

const App = () => {
    const [order, setOrder] = useState({});

    const addItem = itemKey => {
        const orderCopy = {...order};

        if (itemKey in orderCopy) {
            orderCopy[itemKey]++;
        } else {
            orderCopy[itemKey] = 1;
        }

        setOrder(orderCopy);
    };

    const removeItem = itemKey => {
        if (itemKey in order) {
            const orderCopy = {...order};

            if (orderCopy[itemKey] === 0) return;
            orderCopy[itemKey]--;
            setOrder(orderCopy);
        }
    };

    return (
        <div className="container mt-3">
            <div className="row">
                <div className="col-4">
                    <p> <strong>Order Details:</strong></p>
                    <div className="card">
                        <div className="card-body">
                            <OrderSum order={order} remove={removeItem}/>
                        </div>
                    </div>
                </div>

                <div className="col">
                    <p> <strong>Add items:</strong></p>
                    <div className="card">
                        <div className="card-body">
                            <div className="row">
                                {Object.keys(items).map(itemKey => (
                                    <div className="col-md icon" key={itemKey}>
                                        <ItemOrder item={items[itemKey]} add={() => addItem(itemKey)}/>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default App;
