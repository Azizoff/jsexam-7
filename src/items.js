import hamburgerImage from './assets/images/humburger.jpeg';
import cheeseburgerImage from './assets/images/cheeseburger.jpeg';
import friesImage from './assets/images/fries.jpeg';
import colaImage from './assets/images/cola.jpeg';
import coffeeImage from './assets/images/coffee.jpeg';
import teaImage from './assets/images/tea.jpeg';

const items = {
    hamburger: {
        title: 'Hamburger',
        price: 80,
        icon: hamburgerImage
    },
    cheeseburger: {
        title: 'Cheeseburger',
        price: 90,
        icon: cheeseburgerImage
    },
    fries: {
        title: 'Fries',
        price: 45,
        icon: friesImage
    },
    cola: {
        title: 'Coca-Cola',
        price: 70,
        icon: colaImage
    },
    coffee: {
        title: 'Coffee',
        price: 70,
        icon: coffeeImage
    },
    tea: {
        title: 'Tea',
        price: 50,
        icon: teaImage
    }
};

export default items;